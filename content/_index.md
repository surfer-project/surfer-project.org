+++
title = "Surfer"
template = "hero.html"
+++

## Installation

If you just want to try things out, you can try surfer right in the web-browser
at <https://app.surfer-project.org/>. However, performance is a bit worse than
native, and some features are missing so for day-to-day use it is recommended
to install the native version.

The preferred way to install Surfer to build it from source which should work on
Linux, Mac and Windows.

__You can also download pre-built binaries for [Linux](https://gitlab.com/api/v4/projects/42073614/jobs/artifacts/main/raw/surfer_linux.zip?job=linux_build) and [Windows](https://gitlab.com/api/v4/projects/42073614/jobs/artifacts/main/raw/surfer_win.zip?job=windows_build)__


Note that sometimes Windows Defender has been known to report Surfer [and other rust projects](https://github.com/cargo-bins/cargo-binstall/issues/945) as a trojan. If in doubt, please use [Virus total](https://www.virustotal.com/) to check.

## Development Information

Contributions to Surfer are very welcome! See the
[list of issues](https://gitlab.com/surfer-project/surfer/-/issues) if you do not have any
suggestions on your own. Some basic [development information](https://gitlab.com/surfer-project/surfer/-/wikis/home)
is available.


<a href="https://liu.se/en/organisation/liu/isy">
    <img class="h-16 block dark:hidden" src="/liu-primary-black.svg" alt="Linköping University logo">
    <img class="h-16 hidden dark:block" src="/liu-primary-white.svg" alt="Linköping University logo">
</a>

Surfer is currently being developed as an Open Source project at the
[Division of Electronics and Computer Engineering](https://liu.se/en/organisation/liu/isy/elda),
[Department of Electrical Engineering](https://liu.se/en/organisation/liu/isy),
[Linköping University](https://liu.se), Sweden.

For collaboration, contact [Frans Skarman](mailto://frans.skarman@liu.se) or
[Oscar Gustafsson](mailto:oscar.gustafsson@liu.se).

Thanks to [everyone who has contributed!](https://surfer-project.org/thanks/)


## License

Surfer is licensed under the [EUPL-1.2 license](LICENSE.html) ([https://interoperable-europe.ec.europa.eu/collection/eupl/introduction-eupl-licence](https://interoperable-europe.ec.europa.eu/collection/eupl/introduction-eupl-licence)).
