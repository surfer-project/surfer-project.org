---
---
Surfer would not be possible without these amazing people who have contributed to the project ❤️

- Alejandro Tafalla
- Andreas Wallner
- Ben Mattes Krusekamp
- Bracket Master
- Christian
- Christian Dattinger
- Daniel Grosse
- Felix Roithmayr
- Francesco Urbani
- Frans Skarman
- Gustav Sörnäs
- Hugo Lundin
- James Connolly
- Kacper Uminski
- Kevin Läufer
- Lars Kadel
- Lucas Klemmer
- Lukas Scheller
- Matt Taylor
- Oscar Gustafsson
- Theodor Lindberg
- Todd Strader
- Tom Verbeure
- TopTortoise
- Verneri Hirvonen
- Øystein Hovind
 
