# surfer-project.org

Main website for the Surfer Project.

## Development

The website is deployed to Gitlab Pages by Gitlab CI. To develop locally:

- Install the Zola static site generator: https://www.getzola.org/ .
- Install the tailwindcss CLI binary: https://tailwindcss.com/blog/standalone-cli .
- Setup the Tailwind watcher: `tailwindcss -i styles/styles.css -o static/styles/styles.css --watch`.
- In another shell, run Zola: `zola serve`.
- Open http://127.0.0.1:1111 in your browser.

## Theme

This repository also exports a Zola theme that is used for
https://blog.spade-lang.org . The following template HTML-files are exported:

- base.html: the base HTML structure with `<head>`, `<body>`, `<footer>`, table
  of contents, etc. How to work with the structure is explained in more detail
  below.
- page.html and section.html: the templates that are chosen by default for
  `content/**/*.md`-files.

Since this website uses Tailwind, any dependant sites also need to setup Tailwind.
See the file `THEME.md` for more information.

### Custom HTML templates

On https://spade-lang.org , the first page is a hero page that is rendered using
`templates/hero.html`. `hero.html` extends `section.html` which sets the title,
header and section content. The hero-part of the hero (the big initial frame) is
put as the `frontmatter`.

The idea is that _every_ page needs to show its content. For "normal" pages and
sections, this is the only thing that is rendered. Pages that need to be custom
can then override `page.html` or `section.html` as they see fit. Also note that
you can disable the table of contents for your template by writing an empty toc
block:

```
{% block toc %}
{% endblock toc %}
```

You can also override `base.html` directly. You should only have to do that if
you don't want to render text content in any way, for example for an error page
or something.

## Acknowledgements

The initial version of the website is copied from https://spade-lang.org/
The visual design takes a lot of inspiration from [the Juice theme for
Zola](https://github.com/huhu/juice).

## License

Both the website and the theme is licensed under either of

- Apache License, Version 2.0 (LICENSE-APACHE or http://www.apache.org/licenses/LICENSE-2.0)
- MIT license (LICENSE-MIT or http://opensource.org/licenses/MIT)

at your option.
