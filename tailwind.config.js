/** @type {import('tailwindcss').Config} */

module.exports = {
  content: ["./templates/**/*.html"],
  darkMode: 'class',
  /* Classes to include even if they're not found in the content. */
  safelist: [
    'dark:block',
    'dark:hidden',

    'block',
    'hidden',

    'h-16',
  ],
  /* Theme modifications */
  theme: {
    extend: {
      fontFamily: {
        sans: ['"Atkinson Hyperlegible"']
      },
      /* Defaults: https://github.com/tailwindlabs/tailwindcss-typography/blob/master/src/styles.js */
      typography: (theme) => ({
        DEFAULT: {
          css: {
            /* Remove backticks around inline code. */
            'code::before': {
              content: 'unset',
            },
            'code::after': {
              content: 'unset',
            },
          },
        },
        /* More text contrast.
         * TODO: Global style that can be used outside prose too.
         */
        neutral: {
          css: {
            '--tw-prose-body': theme.colors.neutral[900],
            '--tw-prose-headings': theme.colors.black,
            '--tw-prose-invert-body': theme.colors.neutral[200],
          },
        },
      }),
    },
  },
  variants: {},
  plugins: [
    require('@tailwindcss/typography'),
  ],
};
